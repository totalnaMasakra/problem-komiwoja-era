#!/usr/bin/python3
""" rozwiazywanie problemu komiwojazera """

from random import randint
from time import sleep
import pygame

from sposobyNaKomiwojazera import pom, dfs

############################################
## ARGUMENTY PROGRAMU ######################
matrixSize = 20     # wielkość planszy #####
cityQuant = 9       # ilość miast ##########
#### wybierz algorytm ######################
alg = dfs # deep first serch
############################################

## inicjalizacja okna
pygame.init()
screen = pygame.display.set_mode(((matrixSize+2)*20, (matrixSize+2)*20+50))
pygame.display.set_caption("Problem komiwojażera")
font = pygame.font.SysFont("monospace", 12)
screen.fill((255, 255, 255))
pygame.display.flip() # renderuj

## generowanie miast ##
cities = [] # tutaj mamy wszystkie miasta
for i in range( cityQuant ):
    #   (      współrzędna x,          współrzędna y     )
    x = ( randint(0, matrixSize), randint(0, matrixSize) )
    while x in cities:
        x = ( randint(0, matrixSize), randint(0, matrixSize) )
    cities.append( x )

## rysowanie miast na mapie
for i in cities:
    color = ( randint(0,200),randint(0,200),randint(0,200) )
    pygame.draw.circle( screen, color, ( pom(i[0]), pom(i[1]) ), 5, 5 )
    label = font.render( str( i ) , 1, color )
    screen.blit(label, ( pom(i[0])-20, pom(i[1])-17 ) )
label = font.render( "start", 1, (255,0,0) )
screen.blit(label, ( pom(cities[0][0])+3, pom(cities[0][1])+2 ) )

pygame.display.flip() # renderuj

alg = alg( cities )

opis = "użyta metoda: " + alg.name
label = font.render( opis, 1, (0,0,0) )
screen.blit(label, ( 1, (matrixSize+2)*20+4 ) )

opis = "czas działania: " + str(alg.time) + "s"
label = font.render( opis, 1, (0,0,0) )
screen.blit(label, ( 1, (matrixSize+2)*20+15 ) )

opis = "ilość operacji: " + str(alg.oppQuant)
label = font.render( opis, 1, (0,0,0) )
screen.blit(label, ( 1, (matrixSize+2)*20+26 ) )

opis = "długość trasy: " + str( alg.bestLength )
label = font.render( opis, 1, (0,0,0) )
screen.blit(label, ( 1, (matrixSize+2)*20+37 ) )

# rysowanie wybranej najlepszej ścieżki
best = alg.best
for i,(p,d) in enumerate( zip( best[:-1], best[1:] ) ):
    pygame.draw.aaline( screen, (0,0,0),
            ( pom(p[0]), pom(p[1]) ),
            ( pom(d[0]), pom(d[1]) ) )
    label = font.render( str(i+1), 1, (255,0,0) )
    screen.blit(label, ( pom(d[0])+3, pom(d[1])+2 ) )
pygame.display.flip() # renderuj


## główna pętla pygame'a
while True:
    sleep(0.05)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit(0)
        else:
            sleep(0.00001)
            #print(event)
