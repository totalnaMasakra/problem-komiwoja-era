#!/usr/bin/python3
""" rozwiazywanie problemu komiwojazera """

from numpy import inf
from time import time
import math

def pom(i):  # point on map
    """przelicza współrzędne miasta na położenie w pikselach"""
    return (i+1)*20

class dfs(object):
    """ przesukiwanie w glab """
    cities = []
    best = []
    bestLength = inf
    oppQuant = 0
    time = 0
    name = "deep-first-search"

    def __init__(self, cities):  # konstruktor
        self.cities = cities
        beg = time()
        dfs._rec( self, [ self.cities[0] ] )
        end = time()
        self.time = end - beg

    def _rec(self, choosen ):
        """ glowny proces """
        # wywołania rekurencyjne, tworzą ciągi do sprawdzenia
        for m in self.cities:
            if m not in choosen:
                dfs._rec( self, choosen+[m] )
        # jeśli testowy ciąg ma tyle elementów, ile jest miast, sprawdza
        # długość trasy
        if len(self.cities)==len(choosen):
            newLength = 0
            for f,p in zip( choosen[:-1], choosen[1:] ):  # obliczanie długości trasy
                newLength += math.sqrt( abs(   ( p[0] - f[0] )**2    +     ( p[1] - f[1] )**2   ) )
            #print( "zmierzono: " + str(newLength) + "\tukład: " + str( choosen ) )
            self.oppQuant += 1
            if self.bestLength > newLength:  # sprawdzanie, czy trasa jest krótsza
                self.best = choosen
                self.bestLength = newLength
                #print("\t\t\t\t! nowa najlepsza kolejność !")

